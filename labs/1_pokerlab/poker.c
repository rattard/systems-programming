#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "poker.h"

/* converts a hand (of 5 cards) to a string representation, and stores it in the
 * provided buffer. The buffer is assumed to be large enough.
 */
void hand_to_string (hand_t hand, char *handstr) {
	char *p = handstr;
	int i;
	/*unused vars?
	char *val;
	suit_t *suit; */
	for (i=0; i<5; i++) {
		if (hand[i].value < 10) {
			*p++ = hand[i].value + '0';
		} else {
			switch(hand[i].value) {
			case 10: *p++ = 'T'; break;
			case 11: *p++ = 'J'; break;
			case 12: *p++ = 'Q'; break;
			case 13: *p++ = 'K'; break;
			case 14: *p++ = 'A'; break;
			}
		}
		switch(hand[i].suit) {
		case DIAMOND: *p++ = 'D'; break;
		case CLUB: *p++ = 'C'; break;
		case HEART: *p++ = 'H'; break;
		case SPADE: *p++ = 'S'; break;
		}
		if (i<=3) *p++ = ' ';
	}
	*p = '\0';
}

/* converts a string representation of a hand into 5 separate card structs. The
 * given array of cards is populated with the card values.
 */
void string_to_hand (const char *handstr, hand_t hand) {
	int i;
	const char *p=handstr;
	for (i=0; i<5;i++){
		switch (*p++){
		case '2':
			hand[i].value=2;

			break;
		case '3':
			hand[i].value=3;

			break;
		case '4':
			hand[i].value=4;

			break;
		case '5':
			hand[i].value=5;

			break;
		case '6':
			hand[i].value=6;

			break;
		case '7':
			hand[i].value=7;

			break;
		case '8':
			hand[i].value=8;

			break;
		case '9':
			hand[i].value=9;

			break;
		case 'T':
			hand[i].value=10;

			break;
		case 'J':
			hand[i].value=11;

			break;
		case 'Q':
			hand[i].value=12;

			break;
		case 'K':
			hand[i].value=13;

			break;
		case 'A':
			hand[i].value=14;

			break;
		}
		switch((int)*p++){
		case 'D': hand[i].suit= DIAMOND; break;
		case 'C': hand[i].suit= CLUB; break;
		case 'H': hand[i].suit= HEART; break;
		case 'S': hand[i].suit= SPADE; break;
		}
		p++;//advances P past space in string
	}
}
/* sorts the hands so that the cards are in ascending order of value (two
 * lowest, ace highest */
void sort_hand (hand_t hand) {
	int i,j;
	for(i=0;i<5;i++){
		for(j=0;j<4;j++){
			if(hand[j].value>hand[j+1].value){
				card_t temp=hand[j+1];
				hand[j+1].value=hand[j].value;
				hand[j+1].suit=hand[j].suit;
				hand[j].value=temp.value;
				hand[j].suit=temp.suit;
			}
		}
	}

}

int count_pairs (hand_t hand) {
	//return 1 for 1 pair, 2 for 2 pair, 0 for no pairs
	//in case of pair versus pair comparison, you need to know high card of pair.
	//if tie goes to high card
	int hitCounter[15];
	int i;
	int numpairs=0;
	for(i=0;i<15;i++){
		hitCounter[i]=0;
	}
	for(i=0;i<5;i++){
		hitCounter[hand[i].value]++;//adds a hit for each hand
	}
	for(i=0;i<15;i++){
		if(hitCounter[i]==2){
			numpairs++;
		}
		if(hitCounter[i]==4){
			return 4;// adds 2 for 4 of a kind aka 2 pair
		}
	}
	return numpairs;
}
/****COMPLETE FUNCTIONS:
 *
 * stringtohand
 * handtostring
 * sorthand
 * countpairs
 ***************
 * onepair
 * twopairs
 * threeofakind
 * striaght
 * fullhouse
 * flush
 *
 */
int is_onepair (hand_t hand) {//in case of 2 pair , returns value of lower pair
	int returnValue=0;
	int i, numpairs;
	numpairs=count_pairs(hand);
	if(numpairs>0){
		int hitCounter[15];
		for(i=0;i<15;i++){
			hitCounter[i]=0;
		}
		for(i=0;i<5;i++){
			hitCounter[hand[i].value]++;//adds a hit for each hand
		}
		for(i=14;i>1;i--){
			if(numpairs==1&&hitCounter[i]==2){
				return i;
			}
			if(numpairs==4&&hitCounter[i]==4){
				returnValue=i;
				return i;
			}
			if(numpairs==2&&hitCounter[i]==2){
				if(returnValue==0){
					returnValue+=i;
				}
				else {
					if(returnValue>i){//if anything hasn't been written to variable
						return i;
					}
					else
						return returnValue;//returns greatest valued pair
				}
			}

		}
	}

	return 0;
}

int is_twopairs (hand_t hand) {//returns value of high card of pair
	int numpairs=count_pairs(hand);
	if(numpairs==2){

		int returnValue=0;
		int i;
		int hitCounter[15];
		for(i=0;i<15;i++){
			hitCounter[i]=0;
		}
		for(i=0;i<5;i++){
			hitCounter[hand[i].value]++;//adds a hit for each hand
		}
		for(i=15;i>1;i--){
			if(count_pairs(hand)==1&&hitCounter[i]==2){
				return returnValue;
			}
			if(count_pairs(hand)==2&&hitCounter[i]==4){
				return returnValue;
			}
			if(count_pairs(hand)==2&&hitCounter[i]==2){
				if(returnValue==0){
					returnValue+=i;
				}
				if(returnValue>0){
					if(returnValue<i){//if anything hasn't been written to variable
						returnValue=i;
						return returnValue;
					}
					else
						return returnValue;//returns greatest valued pair
				}
			}

		}
	}


	return 0;
}


int is_threeofakind (hand_t hand) {
	int hitCounter[15];
	int i;
	for(i=0;i<15;i++){
		hitCounter[i]=0;
	}
	for(i=0;i<5;i++){
		hitCounter[hand[i].value]++;//adds a hit for each hand
	}
	for(i=0;i<15;i++){
		if(hitCounter[i]==3){
			return i;
		}
	}
	return 0;
}

int is_straight (hand_t hand) {
	int hitCounter[15];
	int isStraight,previousValue;
	int i;
	isStraight=0;
	for(i=0;i<15;i++){
		hitCounter[i]=0;
	}
	for(i=0;i<5;i++){
		if(hand[i].value==14){
			hitCounter[hand[i].value]++;
			hitCounter[1]++;//adds a hit for 1 for ace as well for straights starting at A2345
		}
		hitCounter[hand[i].value]++;//adds a hit for each hand
	}
	for(i=1;i<15&&isStraight!=5;i++){
		if(hitCounter[i]>0){
			if(previousValue==i-1){
				previousValue=i;
				isStraight++;
				if(isStraight==5){
					return i;
				}
			}
			else{
				isStraight=1;
				previousValue=i;
			}
		}
	}
	return 0;
}

int is_fullhouse (hand_t hand) {
	if(is_onepair(hand)&&is_threeofakind(hand)){
		return 1;
	}
	return 0;
}

int is_flush (hand_t hand) {//returns 1 for true, 0 for false
	int i=0;
	suit_t temp=hand[0].suit;
	for(i=1;i<5;i++){
		if(temp!=hand[i].suit)
			return 0;
	}
	return 1;

}

int is_straightflush (hand_t hand) {
	if(is_straight(hand)&&is_flush(hand)){
		return 1;
	}
	return 0;
}
int is_fourofakind (hand_t hand) {
	if(count_pairs(hand)==4){
		return 1;
	}
	return 0;
}

int is_royalflush (hand_t hand) {
	if(is_straight(hand)==14&&is_flush(hand)){
		return 1;
	}
	return 0;
}

/* compares the hands based on rank -- if the ranks (and rank values) are
 * identical, compares the hands based on their highcards.
 * returns 0 if h1 > h2, 1 if h2 > h1.
 */
int compare_hands (hand_t h2, hand_t h1) {
	int (*compareFunctions[9])(hand_t)={is_onepair, is_twopairs, is_threeofakind, is_straight,
			is_fullhouse, is_flush, is_straightflush, is_fourofakind, is_royalflush };
	sort_hand(h1);
	sort_hand(h2);
	//initialize hand type scoring arrays to zero
	int hand1HandTypes[9]={0, 0, 0, 0, 0, 0, 0 , 0, 0};//to hold the values functions return from checking hands
	int hand2HandTypes[9]={0, 0, 0, 0, 0, 0, 0 , 0, 0};//for second hand that is being compared
	int i;
	for( i=0; i<9; i++){
		hand1HandTypes[i]=compareFunctions[i](h1);
		hand2HandTypes[i]=compareFunctions[i](h2);

	}
	i--;
	while(i>=0){
		if(hand1HandTypes[i]!=hand2HandTypes[i]){
			if(hand1HandTypes[i]>hand2HandTypes[i]){
				return 1;
			}
			if(hand1HandTypes[i]<hand2HandTypes[i]){
				return 0;
			}
		}
		i--;
	}
	return compare_highcards(h1,h2);
}

/* compares the hands based solely on their highcard values (ignoring rank). if
 * the highcards are a draw, compare the next set of highcards, and so forth.
 */
int compare_highcards (hand_t h1, hand_t h2) {
	int hitCounter1[15];
	int hitCounter2[15];
	int i;
	for(i=0;i<15;i++){
		hitCounter1[i]=0;
		hitCounter2[i]=0;

	}
	for(i=0;i<5;i++){
		hitCounter1[h1[i].value]++;//adds a hit for each hand
		hitCounter2[h2[i].value]++;//adds a hit for each hand

	}
	for(i=14;i>=0;i--){
		if(hitCounter1[i]>hitCounter2[i]){
			if(hitCounter1[i]>1){

				return compare_hands(h2,h1);
			}
			else{
				return 1;
			}
		}
		if(hitCounter1[i]<hitCounter2[i]){
			if(hitCounter2[i]>1){
				return 0;
				//return compare_hands(h2,h1);
			}
			else{
				return 0;
			}
		}

	}
	return 0;
}
