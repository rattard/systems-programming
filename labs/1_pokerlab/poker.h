typedef enum { DIAMOND, CLUB, HEART, SPADE } suit_t;

typedef struct {
	int value;/* 2-10 for number cards, 10-13 for face cards, 14 for ace */
	suit_t suit;
} card_t;

typedef card_t *hand_t; //hand_t is a pointer type which points to the first
//card in the hand array which is created in main by
//card_t hand1[5], hand2[5];

/* convert hand to/from string representation */
void hand_to_string (hand_t, char *);
void string_to_hand (const char *, hand_t);

/* misc. utility functions */
void sort_hand (hand_t);
int count_pairs(hand_t);//use an array of 15 elements to represent each card type. Each hit gets arr[val]++;


/* hand type predicates -- note that these aren't necessarily mutually
 * exclusive! (e.g., a fullhouse is also a threeofakind and a onepair) */
int is_onepair       (hand_t);	//returns value of pair card, in case of 2 pair, returns highest value,
							 	//full house returns only pair not 3 of a kind
int is_twopairs      (hand_t);	//returns high value //complete!
int is_threeofakind  (hand_t);	//returns value of 3 of a kind
int is_straight      (hand_t);	//returns value of high card in straight
int is_fullhouse     (hand_t);	//returns value y+2*x, where y=value of pair, x is value of 3 of a kind
int is_flush         (hand_t);	//returns value of highest card
int is_straightflush (hand_t);	//returns high card
int is_fourofakind   (hand_t);	//returns value of highest card if valid, else 0
int is_royalflush    (hand_t);	//returns 1 or 0

int compare_hands (hand_t, hand_t); //reutrns 1 for hand1>hand2, 0 for hand2>hand1

int compare_highcards (hand_t, hand_t); //returns 1 for hand1>hand2, 0 for hand2>hand1


/*
 * one pair returns value of pair card (1)
 * one pair returns value of highest pair in case of 2 pair (2)

 * one pair only returns valid PAIRS, not 3 of a kind's (3)
 * two pairs returns highest value
 *
 */
