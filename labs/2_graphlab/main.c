#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "graph.h"

int main (int argc, char *argv[]) {
	vertex_t **vlist_head=malloc(sizeof(vertex_t*));
	*vlist_head=NULL;
	int i;
	for(i=1;i<argc;i+=3 ){
		add_edge(&(*vlist_head), argv[i], argv[i+1], atoi(argv[i+2]));
	}

	print_graph(vlist_head);
	print_tour(vlist_head);
	free_graph(vlist_head);
	free(vlist_head);
	return 0;
}

