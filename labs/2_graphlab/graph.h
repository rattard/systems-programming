typedef struct vertex vertex_t;
typedef struct adj_vertex adj_vertex_t;
typedef struct edge edge_t;


struct vertex {
	char *name;
	adj_vertex_t *adj_list;
	vertex_t *next;
	int visited;
	vertex_t *prev;
};

struct adj_vertex {
	vertex_t *vertex;
	int edge_weight;
	adj_vertex_t *next;	
};
struct edge{
	vertex_t *v1;
	vertex_t *v2;
	int weight;
	edge_t *next;
};

void add_edge (vertex_t **vtxhead, char *v1_name, char *v2_name, int weight);
void free_graph(vertex_t **vtxhead);
void print_graph(vertex_t **vtxhead);
void print_tour(vertex_t **vtxhead);
int isMember(vertex_t **vtx1, vertex_t** vtx2, vertex_t *toured[], int numV, int numT);
int isShortest(vertex_t **vtx1, vertex_t **vtx2);
