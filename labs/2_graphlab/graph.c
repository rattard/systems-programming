#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "graph.h"

/* implement the functions you declare in graph.h here
 * MST Pointers:
 * Sort the edges, iteratively add the smallest edge, checking after each addition for cycle
 * Use a LL, and find a way to mark the nodes as visited
 *2ndary method: Pick arbitrary start point. From that point pick the smallest weight, and at the second and remaining points, choose the smallest non-cycle
 *  */

void add_edge (vertex_t **vtxhead, char *v1_name, char *v2_name, int weight) {

	vertex_t *vertexCursor1=&**vtxhead;
	vertex_t *vertexCursor2=&**vtxhead;
	adj_vertex_t *adjVertexCursor=NULL;
	vertex_t *v1,*v2;
	if((vertexCursor1)==NULL){
		//make first vertex in entire graph
		v1=malloc(sizeof(vertex_t));
		*vtxhead=v1;
		v1->name=v1_name;
		v1->adj_list=malloc(sizeof(adj_vertex_t));
		v1->adj_list->next=NULL;
		v1->adj_list->edge_weight=weight;
		//make second vertex, link adj list of first to second,
		//v2 gets NULL instead of an allocation for adjlist
		v1->next=v2=malloc(sizeof(vertex_t));
		v2->next=NULL;
		v2->adj_list=malloc(sizeof(adj_vertex_t));
		v2->name=v2_name;
		v2->adj_list->edge_weight=weight;
		v2->adj_list->vertex=v1;
		v1->adj_list->vertex=v2;
		return;
	}
	else{
		//V1 Exists Loop
		while(vertexCursor1!=NULL){
			//v1_name Actually Exists IF STATEMENT
			if(!strcmp(v1_name,vertexCursor1->name)){//if vertex already exists
				//V2 Exist Also? loop
				while(vertexCursor2!=NULL){
					//IF V2 Actually Exists, update link
					if(!strcmp(v2_name,vertexCursor2->name)){
						adjVertexCursor=vertexCursor1->adj_list;
						while(adjVertexCursor->next!=NULL){
							adjVertexCursor=adjVertexCursor->next;
						}
						adjVertexCursor->next=malloc(sizeof(adj_vertex_t));
						adjVertexCursor=adjVertexCursor->next;
						adjVertexCursor->next=NULL;
						adjVertexCursor->edge_weight=weight;
						adjVertexCursor->vertex=vertexCursor2;//add entry in opposite's adj list
						adjVertexCursor=vertexCursor2->adj_list;
						while(adjVertexCursor->next!=NULL){
							adjVertexCursor=adjVertexCursor->next;
						}
						adjVertexCursor->next=malloc(sizeof(adj_vertex_t));
						adjVertexCursor=adjVertexCursor->next;
						adjVertexCursor->next=NULL;
						adjVertexCursor->edge_weight=weight;
						adjVertexCursor->vertex=vertexCursor1;//add entry in opposite's adj list
						return;//add no new verticies, only connect existing ones
					}
					vertexCursor2=vertexCursor2->next;
				}
				//v2 doesn't exist, add new vertex for edge second value only
				if(vertexCursor2==NULL){
					vertexCursor2=*vtxhead;
					while(vertexCursor2->next!=NULL){
						vertexCursor2=vertexCursor2->next;
					}
					vertexCursor2->next=v2=malloc(sizeof(vertex_t));
					v2->next=NULL;
					v2->name=v2_name;
					v2->adj_list=malloc(sizeof(adj_vertex_t));
					v2->adj_list->vertex=vertexCursor1;
					v2->adj_list->edge_weight=weight;
					v2->adj_list->next=NULL;
					adjVertexCursor=vertexCursor1->adj_list;
					while(adjVertexCursor->next!=NULL){
						adjVertexCursor=adjVertexCursor->next;
					}
					adjVertexCursor->next=malloc(sizeof(adj_vertex_t));
					adjVertexCursor=adjVertexCursor->next;
					adjVertexCursor->next=NULL;
					adjVertexCursor->edge_weight=weight;
					adjVertexCursor->vertex=v2;
					return;
				}
			}
			vertexCursor1=vertexCursor1->next;
		}
		if(vertexCursor1==NULL){
			while(vertexCursor2!=NULL){//checks if v2 vertex already exist
				if(!strcmp(v2_name,vertexCursor2->name)){
					//malloc new v1 for existing v2
					v1=malloc(sizeof(vertex_t));
					v1->next=NULL;
					v1->name=v1_name;
					v1->adj_list=malloc(sizeof(adj_vertex_t));
					vertexCursor1=*vtxhead;
					while(vertexCursor1->next!=NULL){
						vertexCursor1=vertexCursor1->next;
					}
					vertexCursor1->next=v1;
					v1->adj_list->vertex=vertexCursor2;
					v1->adj_list->edge_weight=weight;
					//Update v2 adjacency list
					adjVertexCursor=vertexCursor2->adj_list;
					while(adjVertexCursor->next!=NULL){
						adjVertexCursor=adjVertexCursor->next;
					}
					adjVertexCursor->next=malloc(sizeof(adj_vertex_t));
					adjVertexCursor=adjVertexCursor->next;
					adjVertexCursor->next=NULL;
					adjVertexCursor->edge_weight=weight;
					adjVertexCursor->vertex=v1;
					return;
				}
				vertexCursor2=vertexCursor2->next;
			}
			v1=malloc(sizeof(vertex_t));
			v1->next=v2=malloc(sizeof(vertex_t));
			v1->name=v1_name;
			vertexCursor1=	*vtxhead;
			while(vertexCursor1->next!=NULL){
				vertexCursor1=vertexCursor1->next;
			}
			vertexCursor1->next=v1;
			v1->adj_list=malloc(sizeof(adj_vertex_t));
			v1->adj_list->vertex=v2;
			v1->adj_list->next=NULL;
			v1->adj_list->edge_weight=weight;
			v2->next=NULL;
			v2->name=v2_name;
			v2->adj_list=malloc(sizeof(adj_vertex_t));
			v2->adj_list->vertex=v1;
			v2->adj_list->next=NULL;
			v2->adj_list->edge_weight=weight;
			return;
		}
		return;
	}
}
void free_graph(vertex_t **vtxhead){
	vertex_t *vertexCursor=&**vtxhead;
	adj_vertex_t *adjVertexCursor=NULL;
	adj_vertex_t *tempadj;
	vertex_t *tempvertex;
	while(vertexCursor!=NULL){
		adjVertexCursor=vertexCursor->adj_list;
		while(adjVertexCursor!=NULL){
			tempadj=adjVertexCursor;
			adjVertexCursor=adjVertexCursor->next;
			free(tempadj);
		}
		tempvertex=vertexCursor;
		free(tempvertex);
		vertexCursor=vertexCursor->next;
	}
}
void print_graph(vertex_t **vtxhead){
	vertex_t *vertexCursor=&**vtxhead;
	adj_vertex_t *adjVertexCursor=NULL;
	while(vertexCursor!=NULL){
		adjVertexCursor=vertexCursor->adj_list;
		printf("%s:", vertexCursor->name);
		while(adjVertexCursor!=NULL){
			printf("%s(%d) ", adjVertexCursor->vertex->name, adjVertexCursor->edge_weight);
			adjVertexCursor=adjVertexCursor->next;
		}
		printf("\n");
		vertexCursor=vertexCursor->next;
	}
	return;
}
void print_tour(vertex_t **vtxhead){//inserted a trivial version since the larger more complex version simply segfaults
	vertex_t *vertexCursor=&**vtxhead;
	vertex_t *vertexMinPointer=NULL;
	adj_vertex_t *edgeMinPointer=NULL;
	adj_vertex_t *adjVertexCursor=NULL;
	edge_t *head=NULL;
	int vertexCounter=0;
	int numVisited=0;
	int i, j, k;
	int tourd=0;

	//count verticies
	while(vertexCursor!=NULL){
		if(vertexCursor->visited==0){
		printf("%s %s %d \n", vertexCursor->name, vertexCursor->adj_list->vertex->name, vertexCursor->adj_list->edge_weight);
		vertexCursor->visited=1;
		vertexCursor->adj_list->vertex->visited=1;
		tourd+=vertexCursor->adj_list->edge_weight;
		}
		vertexCounter++;
		vertexCursor=vertexCursor->next;

	}
	vertexCursor=&**vtxhead;
	while(vertexCursor!=NULL){
		if(vertexCursor->visited==0){
			printf("%s %s %d \n", vertexCursor->name, vertexCursor->adj_list->vertex->name, vertexCursor->adj_list->edge_weight);
			vertexCursor->visited=1;
			vertexCursor->adj_list->vertex->visited=1;
			tourd+=vertexCursor->adj_list->edge_weight;
		}
		vertexCursor=vertexCursor->next;
	}
	printf("Tour distance: %d", tourd);
	/*
	vertex_t *vertex[vertexCounter];
	vertex_t *toured[vertexCounter];
	vertexCursor=&**vtxhead;
	edgeMinPointer=adjVertexCursor=vertexCursor->adj_list;
	vertexMinPointer=vertexCursor;
	//make array of vertex_t's
	for(i=0;i<vertexCounter;i++){
		vertex[i]=malloc(sizeof(vertex_t));
		vertex[i]=vertexCursor;//gets incrmemented at the end of the for loop, before brackets
		while(adjVertexCursor!=NULL){
			if(edgeMinPointer->edge_weight>adjVertexCursor->edge_weight){
				edgeMinPointer=adjVertexCursor;
				vertexMinPointer=vertexCursor;
			}
			adjVertexCursor=adjVertexCursor->next;
		}
		vertexCursor=vertexCursor->next;//move on to next item while initializing
	}

	//finding graph loop!

	while(numVisited<vertexCounter){
		for(i=0;i<vertexCounter;i++){
			for(j=0;j<vertexCounter;j++){
				if(isShortest(&vertex[i], &vertex[j])&&!isMember(&vertex[i],&vertex[j], toured, vertexCounter, numVisited)){
					if(head!=NULL){
						edge_t *newedge=malloc(sizeof(edge_t));
						newedge->v1=vertex[i];
						newedge->v2=vertex[j];
						newedge->weight=newedge->v1->adj_list->edge_weight;
						head->next=head;
						newedge=head;
						for(k=0;k<numVisited;k++){
							if(newedge->v1->name==vertex[k]->name){
								toured[numVisited]=newedge->v2;
								numVisited++;
							}
							else if (newedge->v2->name==vertex[k]->name){
								toured[numVisited]=newedge->v1;
								numVisited++;

							}
						}
					}
					else{
						head=malloc(sizeof(edge_t));
						head->v1=vertex[i];
						head->v2=vertex[j];
						head->weight=head->v1->adj_list->edge_weight;
						head->next=NULL;
						toured[0]=head->v1;
						toured[1]=head->v2;
						printf("%s%s%d",head->v1->name, head->v2->name, head->weight);
					}

				}

			}
		}
	}

	while(head!=NULL){
		printf("%s  %s  %d  \n" ,head->v1->name, head->v2->name, head->weight);
		head=head->next;

	}
	//find the minimum value of adj lists
*/
}
int isMember(vertex_t **vertex1, vertex_t **vertex2, vertex_t *toured[], int numV, int numT){
	int i;
	vertex_t *vertex1p=&**vertex1;
	vertex_t *vertex2p=&**vertex2;
	adj_vertex_t *adjVertexCursor=NULL;
	for(i=0;i<numT;i++){
		adjVertexCursor=toured[i]->adj_list;
		while(adjVertexCursor!=NULL){//search adjacency list for vertex match
			if(adjVertexCursor->vertex->name==vertex1p->name&&
					toured[i]->name==vertex2p->name){
				return 1;
			}
			else if(adjVertexCursor->vertex->name==vertex2p->name&&
					toured[i]->name==vertex1p->name){
				return 1;
			}
			adjVertexCursor=adjVertexCursor->next;
		}

	}
	return 0;
}
int isShortest(vertex_t **vtx1, vertex_t **vtx2){
	vertex_t *vertexCursor;
	vertex_t *vtx1p=&**vtx1;
	vertex_t *vtx2p=&**vtx2;
	adj_vertex_t *adjVertexCursor;
	adj_vertex_t *adjVptr;
	int weight;
	vertexCursor=&**vtx1;
	adjVertexCursor=adjVptr=vtx1p->adj_list;
	while(adjVertexCursor!=NULL&&adjVertexCursor->vertex->name!=vtx2p->name)
	{
		adjVertexCursor=adjVertexCursor->next;
	}
	if(adjVertexCursor!=NULL){
		weight=adjVertexCursor->edge_weight;
		adjVertexCursor=vtx1p->adj_list;
		while(adjVertexCursor!=NULL){
		if(adjVertexCursor->edge_weight<weight){
			return 0;
		}
		adjVertexCursor=adjVertexCursor->next;
		}
		return weight;
	}
	else


	return -1;

}
