/*
 * mm.c Ryan Attard Implementation
 * Heavily taking from the 'bin' DLL concept applied to free blocks from Doug Lea's Malloc.
 * I used his suggested bin sizing, with <512 each getting a bin, and >512 getting a 512 align bin.
 * I am currently aligning all blocks >512 to a 512 block.
 * I should probably change it so that the bins are 512 blocks, not the entire block.
 * I am not copying source code, any	� collisions are entirely that, coincidental.
 * I have rewritten this code using Doug Lea's writeup at gee.cs.oswego.edu/dl/html/malloc.html
 *
 * This is also the Windows NT allocator implementation, with a slight modification (which I might take into account)
 * from:http://msdn.microsoft.com/en-us/library/ms810466.aspx
 *
 *The implementation (Windows NT version 4.0 and later) uses 127 free lists of 8-byte aligned blocks ranging
 *The from 8 to 1,024 bytes and a grab-bag list. The grab-bag list (free list[0]) holds blocks greater than
 *The 1,024 bytes in size. The free list contains objects linked together in a doubly linked list. By default,
 *The  the Process heap performs coalescing operations. (Coalescing is the act of combining adjacent free blocks
 *to build a larger block.) Coalescing costs additional cycles but reduces internal fragmentation of heap blocks.

 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include "mm.h"
#include "memlib.h"

#define MINMEMLOC ((void*)0x5800)
/* single word (4) or double word (8) alignment */
#define ALIGNMENT 8
#define TAGSIZE (sizeof(aheader_t))
#define MINBLOCKSIZE ((int)(TAGSIZE+ALIGNMENT))
#define BIGBLOCKSIZE (4096)
#define NUM8BINS (BIGBLOCKSIZE/ALIGNMENT)
#define NUMBIGBINS 24
#define NUMBINS (NUM8BINS+NUMBIGBINS)//0-128 for payload<1024, 25for < 40,
/* rounds up to the nearest multiple of ALIGNMENT, or multiple
 * of BIGPAYLOADSIZE bytes for large payloads in accordance with bin structure */
#define ALIGN(size) ((size) > BIGBLOCKSIZE ? ((size) > MINBLOCKSIZE*NUMBIGBINS ? ((((size+MINBLOCKSIZE)|(BIGBLOCKSIZE-1))+1)&~0x7) : (((size+MINBLOCKSIZE)|(BIGBLOCKSIZE-1))+1) & ~0x7 ): (((size) + (MINBLOCKSIZE-1)) & ~0x7))

//Find larger address
#define MAX(x, y) ((x) > (y)? (x) : (y))
//inline valid check
typedef struct fheader fheader_t;

void *heapstart;
void *heapend;
fheader_t **BINSTART;
int REALLOCING=0;

#define HEAPSTART heapstart//points to first block (all valid)
#define HEAPEND (mem_heap_hi())//points to start of 16 byte block with size/status=free and size in second block
#define HEAPSIZE (((char*)HEAPEND)-((char*)BINSTART))
#define TOTSIZE (HEAPEND-BINSTART)

//BIN Definitions: For free block definitions find the lookup for the DLL of free blocks of given binsize
#define BIN(x)  ((x) < 0 ? 0 : ((x) > (BIGBLOCKSIZE+(2*TAGSIZE)) ? ((x) > NUMBIGBINS*BIGBLOCKSIZE ? (NUMBINS-1) : (BIGBLOCKSIZE/ALIGNMENT)-(MINBLOCKSIZE/ALIGNMENT)+ ((x)/BIGBLOCKSIZE)+1) : ((x)/ALIGNMENT-(MINBLOCKSIZE/ALIGNMENT))))
typedef struct aheader aheader_t;
typedef struct ffooter ffooter_t;

struct fheader  {
	long size;
	fheader_t *next;
};
struct ffooter {
	long size;
	ffooter_t *next;
};
struct aheader  {
	long size;
};
//helper functions
//used in malloc  functions
void *bestfit(size_t size);
void *bigfit(size_t size);
void coalesce(void *ptr);
void join(void *orig,void*next);
void activecollect(int bigblock);
void *findfit();
void *breakupfit(int size);
void split(void *p,int size);
void macroTest();
/* 
 * mm_init - initialize the malloc package.
 */
int mm_init(void)
{
	//macroTest();
	BINSTART=mem_sbrk(ALIGN(sizeof(fheader_t*[NUMBINS])));
	HEAPSTART=mem_heap_hi()+ALIGN(sizeof(fheader_t*[NUMBINS]));
	int i;
	for(i=0;i<NUMBINS+1;i++){
		BINSTART[i]=NULL;
	}
	return 0;
}

/* 
 * mm_malloc - Allocate a block by incrementing the brk pointer.
 *     Always allocate a block whose size is a multiple of the alignment.
 */
void *mm_malloc(size_t size)
{
	long newsize = ALIGN(size + TAGSIZE);
	void *tp=bestfit(newsize);
	if(tp>MINMEMLOC){
		if(mem_heap_hi()>tp){
			((aheader_t*)tp)->size=(newsize+1);
			return (void*)(((char*)tp)+TAGSIZE);
		}
		else
		{
			return NULL;

		}
	}
	else if(REALLOCING){
			tp=breakupfit(newsize);
	}
	tp=mem_sbrk(newsize);
	if((long)tp>(long)MINMEMLOC&&mem_heap_hi()>=(tp+newsize-1)){
		((aheader_t*)tp)->size=(newsize+1);//assign at address of tp->size newsize
		return (void*)(((char*)tp)+TAGSIZE);//return start of payload
	}
	else{
		return NULL;
	}

}

/*
 * mm_free - Freeing a block adds header tags to blocks
 */
void mm_free(void *ptr)
{
	ptr=((char*)ptr-ALIGNMENT);
	((aheader_t*)ptr)->size--;//assign even values for free blocks
	if((void*)BINSTART[BIN(((aheader_t*)ptr)->size)]<MINMEMLOC){//Check if Bin is empty
		((fheader_t*)(ptr))->next=NULL;
	}
	else{
		((fheader_t*)(ptr))->next=BINSTART[BIN(((aheader_t*)ptr)->size)];//assign old next in bin, regardless of null
	}
	BINSTART[BIN(((aheader_t*)ptr)->size)]=(fheader_t*)ptr;//assign the freed blocks bin ptr to point to it
	if(REALLOCING){
	coalesce(ptr);
	}
	return;
}
void coalesce(void *ptr)
{

	void *original=ptr;
	ptr=((char*)ptr+(((fheader_t*)(ptr))->size-1));
	if(ptr<HEAPEND&&!(((fheader_t*)(ptr))->size)%2){
		join(original,ptr);
	}
	return;
}
void join(void*original,void*next){
	if((!(((fheader_t*)(original))->size)%2)&&(!(((fheader_t*)(next))->size)%2)){
		printf("Joining");
		(((fheader_t*)(original))->size)=((((fheader_t*)(original))->size)+((fheader_t*)(next))->size);

	}
	return;
}

void activecollect(int bigblock){
	int i;
	for(i=(NUM8BINS-1);i<BIN(bigblock);i++){
		fheader_t *cursor=BINSTART[i];
		while((void*)cursor>MINMEMLOC){
			coalesce(cursor);
			cursor=cursor->next;
		}
	}
}
/*
 * mm_realloc - Implemented simply in terms of mm_malloc and mm_free
 */
void *mm_realloc(void *ptr, size_t size)
{
	REALLOCING++;
	void *orig=ptr;
	ptr=((char*)ptr-TAGSIZE);
	((fheader_t*)ptr)->size+=-1;
	if(((char*)ptr+(((fheader_t*)ptr)->size-1))==HEAPEND){
		int newsize=ALIGN(size-((fheader_t*)ptr)->size);
		if(mem_sbrk(newsize)>MINMEMLOC){
			((fheader_t*)ptr)->size+=ALIGN(size-((fheader_t*)ptr)->size)+1;
			ptr=((char*)ptr+TAGSIZE);
			return orig;
		}
	}
	int newsize = ALIGN(size + TAGSIZE);
	void *oldptr = ptr;
	int oldsize=((fheader_t*)ptr)->size;
	((fheader_t*)oldptr)->next=BINSTART[BIN(oldsize)];
	BINSTART[BIN(oldsize)]=(fheader_t*)oldptr;
	void *tp=bestfit(newsize);
	if(tp>MINMEMLOC){
		if(mem_heap_hi()>tp&&tp==oldptr){
			return (((char*)tp)+ALIGNMENT);
		}
		else if(mem_heap_hi()>tp){
			memcpy((((char*)tp)+ALIGNMENT), (((char*)oldptr)+ALIGNMENT), oldsize);
			mm_free(oldptr);
			return (((char*)tp)+ALIGNMENT);

		}
		else
		{
			return NULL;


		}
	}
	tp=mem_sbrk(newsize);
	if((long)tp>(long)MINMEMLOC&&mem_heap_hi()>=(tp+newsize-1)){
		((aheader_t*)tp)->size=newsize+1;//assign at address of tp->size newsize
	}
	else if ((long)tp<=(long)MINMEMLOC){
		//If all else fails, actively garbage collect on all blocks
		activecollect(newsize);
		tp=findfit(newsize);
		if(mem_heap_hi()>tp&&tp==oldptr){
			return (((char*)tp)+ALIGNMENT);
		}
		else if ((long)tp<(long)MINMEMLOC){
			//If all else fails, actively garbage collect on all blocks
			return NULL;

		}
	}
	((aheader_t*)tp)->size=newsize+1;//assign at address of tp->size newsize
	memcpy((((char*)tp)+ALIGNMENT), (((char*)oldptr)+ALIGNMENT), oldsize);
	mm_free(oldptr);
	return (((char*)tp)+ALIGNMENT);
}
void *bestfit(size_t size){
	fheader_t *fit=BINSTART[BIN((int)size)];

	if((long)fit>(long)MINMEMLOC){//checks for low mem locations and free block
		if((long)BINSTART[BIN(size)]->next>(long)MINMEMLOC){
			BINSTART[BIN(size)]=BINSTART[BIN(size)]->next;//is next block available to be added
			return fit;
		}
		else{
			BINSTART[BIN(size)]=NULL;//it is transitioning to NULL, assign it, not the next ptr
			return fit;
		}
	}
	else if(BIN(size)==NUMBINS-2){
		//untested
		fit=bigfit(size);
	}

	return fit;
}
void *findfit(size){
	int i;
	for(i=(BIN(size));i<NUMBINS-2;i++){
		fheader_t *fit=BINSTART[i];
		if((long)fit>(long)MINMEMLOC){
			if((void*)BINSTART[i]>MINMEMLOC){
				BINSTART[i]=BINSTART[i]->next;//is next block available to be added
			}
			else{
				BINSTART[i]=(fheader_t*)NULL;
			}
			split(fit,size);
			return (void*)fit;
		}
	}
	return NULL;
}
void split(void *p,int payload){
	int blocksize=((fheader_t*)p)->size;
	if((blocksize-payload)>MINBLOCKSIZE){
		((fheader_t*)p)->size=(ALIGN(payload+TAGSIZE));
		p=((char*)p+(ALIGN(payload+TAGSIZE)));
		((fheader_t*)p)->size=(ALIGN((blocksize-payload)+TAGSIZE));
		p=((char*)p-(ALIGN(payload+TAGSIZE)));
		return;
	}
	else{
		return;
	}
}
void *breakupfit(int blocksize){
	int i,j;
	fheader_t *bigblock;
	for(i=BIN(blocksize)+1;i<NUMBINS-1;i++){
		if((long)BINSTART[i]>(long)MINMEMLOC){
			bigblock=BINSTART[i];
			BINSTART[i]=bigblock->next;
			int bigblockstartsize=bigblock->size;
			for(j=0;j<((bigblockstartsize)-blocksize);j+=blocksize){
				printf("break %d of %d \n",bigblockstartsize, blocksize	);
				bigblock->size=blocksize;
				bigblock->next=BINSTART[BIN(blocksize)];
				BINSTART[BIN(blocksize)]=bigblock;
			}

		return (void*)bigblock;
		}
	}

	return NULL;
}
void *bigfit(size_t size){
	fheader_t *prev=BINSTART[BIN((int)size)];
	if((void*)prev>MINMEMLOC&&prev->size>size){
		BINSTART[BIN(size)]=BINSTART[BIN(size)]->next;
		return prev;
	}
	fheader_t *binp=prev;
	printf("Big ol' bin:%d/%d+%d heapsize:%li\n", BIN((int)size),NUMBINS,(int)size, HEAPSIZE);
	while((void*)binp>MINMEMLOC){
		if(binp->size>size){
			return prev;
		}
		else{
			prev=binp;
			binp=binp->next;
		}
	}
	return (void*)-1;
}

void macroTest(){
	printf("ALIGNMENT:%d bin:%d,align:%d\n",ALIGNMENT,BIN(ALIGN(ALIGNMENT)),ALIGN(ALIGNMENT));
	printf("2*ALIGNMENT:%d bin:%d,align:%d\n",2*ALIGNMENT,BIN(ALIGN(2*ALIGNMENT)),ALIGN(2*ALIGNMENT));
	printf("2*ALIGNMENT-1:%d bin%d,align:%d\n",2*ALIGNMENT-1,BIN(ALIGN(2*ALIGNMENT-1)),ALIGN(2*ALIGNMENT-1));
	printf("BIGBLOCKSIZE-2*ALIGNMENT:%d bin%d,align%d\n",BIGBLOCKSIZE-2*ALIGNMENT,BIN(ALIGN(BIGBLOCKSIZE-2*ALIGNMENT)),ALIGN(BIGBLOCKSIZE-2*ALIGNMENT));
	printf("BIGBLOCKSIZE-ALIGNMENT:%d bin%d,align%d\n",BIGBLOCKSIZE-ALIGNMENT,BIN(ALIGN(BIGBLOCKSIZE-ALIGNMENT)),ALIGN(BIGBLOCKSIZE-ALIGNMENT));
	printf("BIGBLOCKSIZE-(ALIGNMENT-1):%d bin%d,align%d\n",BIGBLOCKSIZE-(ALIGNMENT-1),BIN(ALIGN(BIGBLOCKSIZE-(ALIGNMENT-1))),ALIGN(BIGBLOCKSIZE-(ALIGNMENT-1)));
	printf("BIGBLOCKSIZE:%d bin%d,align%d\n",BIGBLOCKSIZE,BIN(ALIGN(BIGBLOCKSIZE)),ALIGN(BIGBLOCKSIZE));
	printf("BIGBLOCKSIZE+ALIGNMENT:%d bin%d,align%d\n",BIGBLOCKSIZE+ALIGNMENT,BIN(ALIGN(BIGBLOCKSIZE+ALIGNMENT)),ALIGN(BIGBLOCKSIZE+ALIGNMENT));
	printf("2*BIGBLOCKSIZE-ALIGNMENT:%d bin%d,align%d\n",2*BIGBLOCKSIZE-ALIGNMENT,BIN(ALIGN(2*(BIGBLOCKSIZE-ALIGNMENT))),ALIGN(2*(BIGBLOCKSIZE-ALIGNMENT)));
	printf("10*BIGBLOCKSIZE:%d bin%d,align%d\n",10*BIGBLOCKSIZE,BIN(ALIGN(10*BIGBLOCKSIZE)),ALIGN(10*BIGBLOCKSIZE));
	printf("100*BIGBLOCKSIZE+1:%d bin%d,align%d\n",100*BIGBLOCKSIZE+1,BIN(ALIGN(100*BIGBLOCKSIZE+1)),ALIGN(100*BIGBLOCKSIZE+1));
	printf("(NUMBIGBINS-3)*BIGBLOCKSIZE:%d bin%d,align%d\n",(NUMBIGBINS-3)*BIGBLOCKSIZE,BIN(ALIGN((NUMBIGBINS-3)*BIGBLOCKSIZE)),ALIGN((NUMBIGBINS-3)*BIGBLOCKSIZE));
	printf("(NUMBIGBINS-2)*BIGBLOCKSIZE:%d bin%d,align%d\n",(NUMBIGBINS-2)*BIGBLOCKSIZE,BIN(ALIGN((NUMBIGBINS-2)*BIGBLOCKSIZE)),ALIGN((NUMBIGBINS-2)*BIGBLOCKSIZE));
	printf("(NUMBIGBINS-1)*BIGBLOCKSIZE:%d bin%d,align%d\n",(NUMBIGBINS-1)*BIGBLOCKSIZE,BIN(ALIGN((NUMBIGBINS-1)*BIGBLOCKSIZE)),ALIGN((NUMBIGBINS-1)*BIGBLOCKSIZE));
	printf("NUMBIGBINS*BIGBLOCKSIZE:%d bin%d,align%d\n",NUMBIGBINS*BIGBLOCKSIZE,BIN(ALIGN(NUMBIGBINS*BIGBLOCKSIZE)),ALIGN(128*BIGBLOCKSIZE+1));
	printf("BIGBLOCKSIZE*BIGBLOCKSIZE+1:%d bin%d,align%d\n",BIGBLOCKSIZE*BIGBLOCKSIZE+1,BIN(ALIGN(BIGBLOCKSIZE*BIGBLOCKSIZE+1)),ALIGN(BIGBLOCKSIZE*BIGBLOCKSIZE+1));

}

