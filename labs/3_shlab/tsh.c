/* 
 * tsh - A tiny shell program with job control
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

/* Misc manifest constants */
#define MAXLINE    1024   /* max line size */
#define MAXARGS     128   /* max args on a command line */
#define MAXJOBS      24   /* max jobs at any point in time */
#define MAXJID    1<<16   /* max job ID */

/* Job states */
#define UNDEF -1 /* undefined */
#define FG 1    /* running in foreground */
#define BG 0    /* running in background */
#define ST 2    /* stopped */
#define BUILTIN_T_NUM 4/*
 * Jobs states: FG (foreground), BG (background), ST (stopped)
 * Job state transitions and enabling actions:
 *     FG -> ST  : ctrl-z
 *     ST -> FG  : fg command
 *     ST -> BG  : bg command
 *     BG -> FG  : fg command
 * At most 1 job can be in the FG state.
 */

/* Global variables */
extern char **environ;      /* defined in libc */
char prompt[] = "tsh> ";    /* command line prompt (DO NOT CHANGE) */
int verbose = 0;            /* if true, print additional output */
int nextjid = 1;            /* next job ID to allocate */
int numFG = 0;
char sbuf[MAXLINE];         /* for composing sprintf messages */
int emit_prompt = 1; /* emit prompt (default) */
pid_t shpid=-1;

struct job_t {              /* The job struct */
	pid_t pid;              /* job PID */
	int jid;                /* job ID [1, 2, ...] */
	int state;              /* UNDEF -1, BG 0, FG 1, or ST 2 */
	int argc;
	int err;
	char **argv;  			/*Arg variable list*/
};
struct job_t jobs[MAXJOBS]; /* The job list */
/* End global variables */


/* Function prototypes */

/* Here are the functions that you will implement */
void eval(char *cmdline);
int builtin_cmd(char **argv, int argc);
void do_bgfg(char **argv);
void do_bg(char **argv, int argc);
void do_fg(char **argv, int argc);
void waitfg(pid_t pid);

typedef void handler_t(int);
handler_t *Signal(int signum, handler_t *handler);
void sigchld_handler(int sig);
void sigtstp_handler(int sig);
void sigint_handler(int sig);
void sigterm_handler(int sig);
void sigquit_handler(int sig);
void sig_dfl();
void sig_ignore();//sets signals to SIG_IGN
void sig_restore();//sets handlers to our handlers

/* Here are helper routines that we've provided for you */
int parseline(const char *cmdline, char **argv);


void clearjob(struct job_t *job);
void initjobs(struct job_t *jobs);
int maxjid(struct job_t *jobs); 
int addjob(struct job_t jobs[], pid_t pid, int state, int argc, char **argv);
int deletejob(struct job_t *jobs, pid_t pid); 
pid_t fgpid(struct job_t *jobs);
struct job_t *getjobpid(struct job_t *jobs, pid_t pid);
struct job_t *getjobjid(struct job_t *jobs, int jid); 
int pid2jid(pid_t pid); 
int isJob(int jid);
void listjobs(struct job_t *jobs);

void usage(void);
void unix_error(char *msg);
void app_error(char *msg);

/*
 * main - The shell's main routine 
 */
int main(int argc, char **argv) 
{
	shpid=getpid();
	char c;
	char cmdline[MAXLINE];
	/* Redirect stderr to stdout (so that driver will get all output
	 * on the pipe connected to stdout) */
	dup2(1, 2);

	/* Parse the Arguments to Shell Run */
	while ((c = getopt(argc, argv, "hvp")) != EOF) {
		switch (c) {
		case 'h':             /* print help message */
			usage();
			break;
		case 'v':             /* emit additional diagnostic info */
			verbose = 1;
			break;
		case 'p':             /* don't print a prompt */
			emit_prompt = 0;  /* handy for automatic testing */
			break;
		default:
			usage();
			break;
		}
	}



	/* Initialize the job list */
	initjobs(jobs);


	/* Execute the shell's read/eval loop */
	while (1) {
		/* Install the signal handlers */
		sig_restore();
		/* Read command line */
		if (emit_prompt) {
			printf("%s", prompt);
			fflush(stdout);
		}
		if ((fgets(cmdline, MAXLINE, stdin) == NULL) && ferror(stdin))
			app_error("fgets error");
		if (feof(stdin)) { /* End of file (ctrl-d) */
			fflush(stdout);
			exit(0);
		}

		/* Evaluate the command line */
		eval(cmdline);
		fflush(stdout);
		fflush(stdout);
	}

	exit(0); /* control never reaches here */
}

/* 
 * eval - Evaluate the command line that the user has just typed in
 * 
 * If the user has requested a built-in command (quit, jobs, bg or fg)
 * then execute it immediately. Otherwise, fork a child process and
 * run the job in the context of the child. If the job is running in
 * the foreground, wait for it to terminate and then return.  Note:
 * each child process must have a unique process group ID so that our
 * background children don't receive SIGINT (SIGTSTP) from the kernel
 * when we type ctrl-c (ctrl-z) at the keyboard.  
 */
void eval(char *cmdline) 
{
	char **argv=malloc(sizeof(char**));
	int argc=parseline(cmdline, argv);
	if(argv[0]==NULL){
		return;
	}

	if(builtin_cmd(argv, argc)){
		return;
	}

	if(argc<0){
		do_bg(argv, abs(argc));//returns a negative argc for the line in parseline if it is a bg command
		return;

	}
	if(argc>0){

		do_fg(argv,argc);
		return;
	}
	return;
}

/* 
 * parseline - Parse the command line and build the argv array.
 * 
 * Characters enclosed in single quotes are treated as a single
 * argument.  Return negative argc if the user has requested a BG job, positive argc if
 * the user has requested a FG job.  
 */
int parseline(const char *cmdline, char **argv) 
{
	char *buf=malloc(sizeof(char*)*MAXLINE);/* ptr that traverses command line */
	char *delim;                /* points to first space delimiter */
	int argc;                   /* number of args */
	int bg;                     /* background job? */
	strcpy(buf, cmdline);
	buf[strlen(buf)-1] = ' ';  /* replace trailing '\n' with space */
	while (*buf && (*buf == ' ')) /* ignore leading spaces */
		buf++;

	/* Build the argv list */
	argc = 0;
	if (*buf == '\'') {
		buf++;
		delim = strchr(buf, '\'');
	}
	else {
		delim = strchr(buf, ' ');
	}
	while (delim) {
		/*
		 *
$34 = 0x604340 "/bin/echo -e tsh> print \\046 "
(gdb) step
225			argv[argc]=malloc(sizeof(char*));
(gdb) print buf
$35 = 0x604340 "/bin/echo -e tsh> print \\046 "
(gdb) step
226			argv[argc++] = buf;
(gdb) print buf
$36 = 0x604340 "/bin/echo -e tsh> print !"
(gdb) step
227			*delim = '\0';
		 */
		strcat(buf,"\0");
		argv[argc]=malloc(sizeof(char*));
		strncpy(argv[argc++],buf, delim-buf);
		*delim = '\0';
		buf = delim + 1;
		while (*buf && (*buf == ' ')) /* ignore spaces */
			buf++;

		if (*buf == '\'') {
			buf++;
			delim = strchr(buf, '\'');
		}
		else {
			delim = strchr(buf, ' ');
		}
	}
	argv[argc] = NULL;

	if (argc == 0)  /* ignore blank line */
		return argc;

	/* should the job run in the background? */
	if ((bg = (argv[argc-1][0] == '&'&&strlen(argv[argc-1])==1)) != 0) {

		argv[argc] = NULL;
	}
	if(bg){
		return -argc;
	}
	return argc;
}

/* 
 * builtin_cmd - If the user has typed a built-in command then execute
 *    it immediately.  
 */
int builtin_cmd(char **argv, int argc)
{
	if(!strcmp(argv[0], "quit")){
		sigquit_handler(1);
		return 1;
	}
	if(!strcmp("fg",argv[0])||!strcmp("bg",argv[0])){
		if(argc>1){
			do_bgfg(argv);
			return 1;
		}
		else {
			printf("%s: commands requires PID or %%jobid argument\n",argv[0]);
			fflush(stdout);
			return 1;
		}
	}

	if(!strcmp("jobs", argv[0])){
		listjobs(jobs);
		fflush(stdout);
		return 1;

	}
	return 0;
}

/*
 * do_bgfg - Execute the builtin bg and fg commands
 */
void do_bgfg(char **argv) {
	int temp=atoi(argv[1]);

	if(temp==0){
		argv[1][0]=' ';
		temp=atoi(argv[1]);
		if(temp==0){
			printf("%s: argument must be a PID or %%jobid\n",argv[0]);
			fflush(stdout);
			return;
		}
		if(isJob(temp)){
			if(!strcmp(argv[0],"bg"))
			{

				kill(-getjobjid(jobs,temp)->pid, SIGCONT);
				getjobjid(jobs,temp)->state=BG;
				printf("[%d] (%d) ", getjobjid(jobs,temp)->jid, getjobjid(jobs,temp)->pid);
				int j;
				for(j=0;j<getjobjid(jobs,temp)->argc;j++){
					printf("%s ", getjobjid(jobs,temp)->argv[j]);
				}
				printf("\n");
				fflush(stdout);
				fflush(stdout);
				return;
			}
			else
			{
				if(getjobjid(jobs,temp)->state==ST){

					kill(-getjobjid(jobs,temp)->pid, SIGCONT);
					getjobjid(jobs,temp)->state=FG;
					numFG++;
					waitfg(getjobjid(jobs,temp)->pid);
					numFG--;
					return;
				}
				if(getjobjid(jobs,temp)->state==BG){

					getjobjid(jobs,temp)->state=FG;
					numFG++;
					waitfg(getjobjid(jobs,temp)->pid);
					numFG--;
					return;
				}

			}
		}
		else{
			printf("%d: No such process\n",temp);
			fflush(stdout);
			return;
		}
	}
	else{
		int temp=atoi(argv[1]);
		if(!(temp>1&&temp<MAXJOBS)){
			printf("%s: argument must be a PID or %%jobid \n",argv[0]);
			fflush(stdout);
			return;
		}
		if(!strcmp(argv[0],"FG")){
			if(getjobpid(jobs,atoi(argv[1]))!=NULL){
				getjobpid(jobs,atoi(argv[1]))->state=FG;
				numFG++;
				kill(-getjobjid(jobs,atoi(argv[1]))->pid, SIGCONT);
				waitfg(atoi(argv[1]));
				numFG--;
			}
			else{
				return;
			}
		}
		else{
			if(getjobpid(jobs,atoi(argv[1]))!=NULL){
				kill(atoi(argv[1]), SIGCONT);
				getjobpid(jobs,atoi(argv[1]))->state=BG;
			}
			else{
				return;
			}

		}
	}

	return;
}
void do_bg(char **argv, int argc){

	//block signals
	//Fork Child
	//->in child
	//->setpgrp
	//->unblock signals
	//->exec
	//->evaluation of code
	//->terminaton
	//->send SIGCHLD to parent
	//add job
	//unblock signals
	//reap child
	//delete job
	if(argc==1){
		return;
	}
	pid_t curr;
	sig_ignore();
	if((curr=fork())==0){//fork to prepare new process
		sig_dfl();
		setpgrp();
		strcpy("\0",argv[argc-1]);//remove the  &
		if(execve(argv[0], argv, environ)==-1){
			char *temp=malloc(sizeof(char));
			sprintf(temp,"%s: Command not found", argv[0]);
			app_error(temp);
			numFG--;
		}
	}
	if(curr>0){
		addjob(jobs, curr, 0, argc, argv);
		Signal(SIGINT, sigint_handler);
		Signal(SIGTSTP, sigtstp_handler);
		Signal(SIGCHLD, sigchld_handler);
		Signal(SIGTERM, sigterm_handler);
	}
	return;

}
void do_fg(char **argv, int argc){
	//Fork Child
	//->in child
	//->setpgrp
	//->exec
	//->evaluation of code
	//->terminaton
	//->send SIGCHLD to parent
	//add job
	//unblock signals
	//reap child
	//delete job
	pid_t curr;
	sig_ignore();
	if((curr=fork())==0){//fork to prepare new process
		setpgrp();
		sig_dfl();
		if(execve(argv[0], argv, environ)==-1){
			char *temp=malloc(sizeof(char));
			sprintf(temp,"%s: Command not found WTF", argv[0]);
			app_error(temp);
			exit(0);
		}
		else{
			char *temp=malloc(sizeof(char));
			sprintf(temp,"%s: Command not found", argv[0]);
			app_error(temp);
			exit(0);
		}
	}
	if(curr>0){
		//add jobs
		//wait in foreground
		//check if stopped part way thru
		//delete jobs
		//unblock signals
		addjob(jobs, curr, FG, argc, argv);
		numFG++;
		sig_restore();
		waitfg(curr);
		if(fgpid(jobs)==-1){//sigint removes FG job
			Signal(SIGINT, sigint_handler);
			Signal(SIGTSTP, sigtstp_handler);
			Signal(SIGCHLD, sigchld_handler);
			Signal(SIGTERM, sigterm_handler);
			return;
		}
		if(getjobpid(jobs,curr)->state==ST){//stop a job--decrement FG

			Signal(SIGINT, sigint_handler);
			Signal(SIGTSTP, sigtstp_handler);
			Signal(SIGCHLD, sigchld_handler);
			Signal(SIGTERM, sigterm_handler);
			return;
		}

	}
	numFG--;
	return;

}

/*
 * waitfg - Block until process pid is no longer the foreground process
 */
void waitfg(pid_t pid)
{
	sig_restore();
	waitpid(pid,&getjobpid(jobs,pid)->err,WSTOPPED);
	return;

}

/*****************
 * Signal handlers
 *****************/

/*
 * sigchld_handler - The kernel sends a SIGCHLD to the shell whenever
 *     a child job terminates (becomes a zombie), or stops because it
 *     received a SIGSTOP or SIGTSTP signal. The handler reaps all
 *     available zombie children, but doesn't wait for any other
 *     currently running children to terminate.
 */
void sigchld_handler(int sig)
{
	Signal(SIGINT, SIG_IGN);
	Signal(SIGTSTP, SIG_IGN);
	Signal(SIGCHLD, SIG_IGN);
	Signal(SIGTERM, SIG_IGN);
	int i;
	for(i=1;i<MAXJOBS;i++){
		if(jobs[i].pid>0){
			if(jobs[i].state>=0){
				if(WIFEXITED(jobs[i].err)){
					deletejob(jobs, jobs[i].pid);

				}
			}
		}
	}
	Signal(SIGINT, sigint_handler);
	Signal(SIGTSTP, sigtstp_handler);
	Signal(SIGCHLD, sigchld_handler);
	Signal(SIGTERM, sigterm_handler);
	return;
}
/*
 * sigint_handler - The kernel sends a SIGINT to the shell whenver the
 *    user types ctrl-c at the keyboard.  Catch it and send it along
 *    to the foreground job.
 */
void sigint_handler(int sig)
{
	Signal(SIGINT, SIG_IGN);
	Signal(SIGTSTP, SIG_IGN);
	Signal(SIGCHLD, SIG_IGN);
	Signal(SIGTERM, SIG_IGN);
	int pid=fgpid(jobs);


	if(pid>0){
		if(kill(-pid, SIGINT)==-1){
			if(errno==EINVAL){
				printf("Invalid Signal\n" );
			}

			if(errno==EPERM){
				printf("Invalid Permissions\n" );
			}
			if(errno==ESRCH){
				printf("Invalid PID or PGRP\n" );
			}
		}
		waitpid(pid,&getjobpid(jobs,pid)->err,WUNTRACED);
		if(WIFEXITED(getjobpid(jobs,pid)->err)){
			printf("Job [%d] (%d) terminated by signal %d \n",pid2jid(pid),
					pid ,SIGINT);
		}
		fflush(stdout);
		deletejob(jobs,pid);
		return;
	}
	else{
		printf("No FG jobs\n");
		fflush(stdout);
		return;
	}



}

/*
 * sigtstp_handler - The kernel sends a SIGTSTP to the shell whenever
 *     the user types ctrl-z at the keyboard. Catch it and suspend the
 *     foreground job by sending it a SIGTSTP.
 */
void sigtstp_handler(int sig)
{
	sig_ignore();
	//when signaled by 20, it makes the process disappear
	pid_t pid=fgpid(jobs);
	int jid=pid2jid(pid);
	if(pid>0){
		if(WIFEXITED(jobs[jid].err)){
			deletejob(jobs,pid);
			sig_restore();
			return;
		}
		if(WIFSTOPPED(jobs[jid].err)){
			getjobpid(jobs,pid)->state=ST;
			printf("Job [%d] (%d) previously stopped by signal %d \n",jid,
					pid, WSTOPSIG(jobs[jid].err));
			fflush(stdout);
			numFG--;
			sig_restore();
			return;
		}
		//having trouble with it going in here for some reason....
		if(!kill(-pid, SIGTSTP)){
			if(errno==EINVAL){
				printf("Invalid Signal\n" );
			}

			if(errno==EPERM){
				printf("Invalid Permissions\n" );
			}
			if(errno==ESRCH){
				printf("Invalid PID or PGRP\n" );
			}
		}
		getjobpid(jobs,pid)->state=ST;
		printf("Job [%d] (%d) stopped by signal %d\n",jid,
				pid, SIGTSTP);
		fflush(stdout);
		sig_restore();
		return;
	}
	else{
		printf("No FG jobs \n");
		fflush(stdout);
		sig_restore();
		return;
	}
}
void sig_ignore(){
	struct sigaction action,oldaction;
	action.sa_handler = SIG_IGN;
	sigemptyset(&action.sa_mask); /* block sigs of type being handled */
	sigaddset (&action.sa_mask, SIGINT);
	sigaddset (&action.sa_mask, SIGQUIT);
	sigaddset (&action.sa_mask, SIGCHLD);
	sigaddset (&action.sa_mask, SIGTSTP);
	sigaddset (&action.sa_mask, SIGTERM);
	sigprocmask(SIG_BLOCK,&action.sa_mask,&oldaction.sa_mask);

}
void sig_dfl(){
	struct sigaction action,oldaction;
	action.sa_handler = SIG_DFL;
	sigemptyset(&action.sa_mask); /* block sigs of type being handled */
	sigaddset (&action.sa_mask, SIGINT);
	sigaddset (&action.sa_mask, SIGQUIT);
	sigaddset (&action.sa_mask, SIGCHLD);
	sigaddset (&action.sa_mask, SIGTSTP);
	sigaddset (&action.sa_mask, SIGTERM);
	sigprocmask(SIG_UNBLOCK,&action.sa_mask,&oldaction.sa_mask);
	Signal(SIGINT, SIG_DFL);
	Signal(SIGTSTP, SIG_DFL);
	Signal(SIGCHLD, SIG_DFL);
	Signal(SIGTERM, SIG_DFL);
	Signal(SIGQUIT, SIG_DFL);

}
void sig_restore(){
	struct sigaction action,oldaction;
	action.sa_handler = SIG_DFL;
	sigemptyset(&action.sa_mask); /* block sigs of type being handled */
	sigaddset (&action.sa_mask, SIGINT);
	sigaddset (&action.sa_mask, SIGQUIT);
	sigaddset (&action.sa_mask, SIGCHLD);
	sigaddset (&action.sa_mask, SIGTSTP);
	sigaddset (&action.sa_mask, SIGTERM);
	sigprocmask(SIG_UNBLOCK,&action.sa_mask,&oldaction.sa_mask);
	Signal(SIGINT, sigint_handler);
	Signal(SIGTSTP, sigtstp_handler);
	Signal(SIGCHLD, sigchld_handler);
	Signal(SIGTERM, sigterm_handler);
	Signal(SIGQUIT, sigquit_handler);
}
/*
 * sigquit_handler - The driver program can gracefully terminate the
 *    child shell by sending it a SIGQUIT signal.
 */
void sigquit_handler(int sig)
{
	int i;
	for (i = 1; i < MAXJOBS && jobs[i].pid>0; i++) {
		kill(-jobs[i].pid, SIGQUIT);
	}
	switch(sig){
	case 1:
		exit(1);
		return;
		break;
	default:
		printf("Terminating after receipt of SIGQUIT signal\n");
		exit(1);
		return;
		break;
	}
}
void sigterm_handler(int sig){
	exit(0);
}

/*********************
 * End signal handlers
 *********************/
/*********************
 * Start of Signal Helpers
 */


handler_t *Signal(int signum, handler_t *handler)
{
	struct sigaction action, old_action;

	action.sa_handler = handler;
	sigemptyset(&action.sa_mask); /* block sigs of type being handled */
	sigaddset (&action.sa_mask, SIGINT);
	sigaddset (&action.sa_mask, SIGQUIT);

	action.sa_flags = SA_RESTART; /* restart syscalls if possible */

	if (sigaction(signum, &action, NULL) < 0)
		unix_error("Signal error");
	return (old_action.sa_handler);
}


/***********************************************
 * Helper routines that manipulate the job list
 **********************************************/

/* clearjob - Clear the entries in a job struct */
void clearjob(struct job_t *job) {
	job->pid = 0;
	job->jid = 0;
	job->state = UNDEF;
	job->argc=0;
	job->err=0;
	if(job->argv!=NULL){
		free(jobs->argv);
		jobs->argv=NULL;
	}
}

/* initjobs - Initialize the job list */
void initjobs(struct job_t jobs[]) {
	int i;
	jobs=malloc(sizeof(struct job_t[MAXJOBS]));

	for (i = 0; i < MAXJOBS; i++){
		clearjob(&jobs[i]);
	}
}

/* maxjid - Returns largest allocated job ID */
int maxjid(struct job_t *jobs)
{
	int i, max=0;

	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].jid > max)
			max = jobs[i].jid;
	return max;
}

/* addjob - Add a job to the job list */
int addjob(struct job_t jobs[], pid_t pid, int state, int argc, char **argv)
{
	int i;

	if (pid < 1)
		return -1;

	if(numFG){
		return -1;
	}

	for (i = 1; i < MAXJOBS; i++) {
		if (jobs[i].pid == 0) {
			jobs[i].pid = pid;
			jobs[i].state = state;
			jobs[i].jid = nextjid++;
			jobs[i].argc=argc;
			if (nextjid > MAXJOBS)
				nextjid = 1;
			int j;
			jobs[i].argv=malloc(sizeof(char**));
			for(j=0;j<argc;j++){
				jobs[i].argv[j]=malloc(sizeof(char*));
				strcpy(jobs[i].argv[j], argv[j]);
			}
			if(verbose){
				printf("Added job [%d] %d %s\n", jobs[i].jid, jobs[i].pid, *jobs[i].argv);
				fflush(stdout);
			}
			if(jobs[i].state==BG){
				printf("[%d] (%d) ", jobs[i].jid, jobs[i].pid);
				int j;
				for(j=0;j<jobs[i].argc;j++){
					printf("%s ", jobs[i].argv[j]);
				}
				printf("\n");
				fflush(stdout);
				fflush(stdout);

			}
			return 1;
		}
	}
	printf("Tried to create too many jobs\n");
	return 0;
}

/* deletejob - Delete a job whose PID=pid from the job list */
int deletejob(struct job_t *jobs, pid_t pid)
{
	int i;

	if (pid < 1)
		return 0;

	for (i = 0; i < MAXJOBS; i++) {
		if (jobs[i].pid == pid) {
			clearjob(&jobs[i]);
			nextjid = maxjid(jobs)+1;
			return 1;
		}
	}
	return 0;
}

/* fgpid - Return PID of current foreground job, 0 if no such job */
pid_t fgpid(struct job_t *jobs) {
	int i;

	for (i = 1; i < MAXJOBS; i++)
		if (jobs[i].state == FG)
			return jobs[i].pid;
	return -1;
}

/* getjobpid  - Find a job (by PID) on the job list */
struct job_t *getjobpid(struct job_t *jobs, pid_t pid) {
	int i;

	if (pid < 1)
		return NULL;
	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].pid == pid)
			return &jobs[i];
	return NULL;
}

/* getjobjid  - Find a job (by JID) on the job list */
struct job_t *getjobjid(struct job_t *jobs, int jid)
{
	int i;

	if (jid < 1)
		return NULL;
	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].jid == jid)
			return &jobs[i];
	return NULL;
}

/* pid2jid - Map process ID to job ID */
int pid2jid(pid_t pid)
{
	int i;

	if (pid < 1)
		return 0;
	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].pid == pid) {
			return jobs[i].jid;
		}
	return -1;
}
int isJob(int jid){
	if(jobs[jid].pid>0){
		return 1;
	}
	return 0;
}

/* listjobs - Print the job list */
void listjobs(struct job_t *jobs)
{
	int i;

	for (i = 1; i < MAXJOBS; i++) {
		if (jobs[i].pid > 0&&jobs[i].state!=-1) {
			printf("[%d] (%d) ", jobs[i].jid, jobs[i].pid);
			switch (jobs[i].state) {
			case 0:
				printf("Running ");
				break;
			case 1:
				printf("Foreground ");
				break;
			case 2:
				printf("Stopped ");
				break;
			default:
				printf("listjobs: Internal error: job[%d].state=%d ",
						jobs[i].jid, jobs[i].state);
				break;
			}
			int j;
			for(j=0;j<jobs[i].argc;j++){
				printf("%s ", jobs[i].argv[j]);
			}
			printf("\n");
		}
	}
	fflush(stdout);
}
/******************************
 * end job list helper routines
 ******************************/


/***********************
 * Other helper routines
 ***********************/

/*
 * usage - print a help message
 */
void usage(void)
{
	printf("Usage: shell [-hvp]\n");
	printf("   -h   print this message\n");
	printf("   -v   print additional diagnostic information\n");
	printf("   -p   do not emit a command prompt\n");
	exit(1);
}

/*
 * unix_error - unix-style error routine
 */
void unix_error(char *msg)
{
	fprintf(stdout, "%s: %s\n", msg, strerror(errno));
	exit(1);
}

/*
 * app_error - application-style error routine
 */
void app_error(char *msg)
{
	fprintf(stdout, "%s\n", msg);
	exit(1);
}

/*
 * Signal - wrapper for the sigaction function
 */


