/* 
 * hello.c - the quintessential hello world program
 */
#include <stdio.h>

int main () {
  	printf("Ryan Attard\n");
	printf("A20142148\n");
	printf("When does the narwhal bacon?\n");
	printf("Midnight! \n");
  
	return 0;
}
